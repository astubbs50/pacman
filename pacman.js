var canvas;
var context;
var canvasMap;
var contextMap;
var t;
var lastTime;
var dt;
var map;
var mapData;
var pellets;
var pacman;
var ghosts;
var msgLog;
var bCanMove;
var score = 0;
var scoreMultiple = 0;
var images;
var floatingTexts;
var resizeTimer;
var canvasMapCreate;
var canvasCreate;

function Init()
{
	msgLog = [];
	floatingTexts = [];
	dt = 0;
	canvasMapCreate = canvasFactory.Create({aspectWidth: 1, fullscreen: true});
	canvasMap = canvasMapCreate.canvas;
	contextMap = canvasMapCreate.context;
	
	canvasCreate = canvasFactory.Create({aspectWidth: 1, fullscreen: true});
	canvas = canvasCreate.canvas;
	context = canvasCreate.context;
	canvas.style.backgroundColor = "rgba(0, 0, 0, 0)";
	
	ImageLoader.LoadImage("cyan_ghost.png");
	ImageLoader.LoadImage("orange_ghost.png");
	ImageLoader.LoadImage("pink_ghost.png");
	ImageLoader.LoadImage("red_ghost.png");
	ImageLoader.LoadImage("scared_ghost.png");
	ImageLoader.RegisterOnReady(InitGame, true);	
}

function Resize()
{
	clearTimeout(resizeTimer);
	resizeTimer = setTimeout(function () {
		canvasMapCreate.resize();
		canvasCreate.resize();
		mapData.blockWidth = Math.round(canvasMap.width / map.length);
		mapData.blockHeight = Math.round(canvasMap.height / map[0].length); 
		mapData.blockHalfWidth =  mapData.blockWidth / 2;
		mapData.blockHalfHeight = mapData.blockHeight / 2;
		SetPacmanSize();
		for(var i in ghosts)
		{
			SetGhostSize(ghosts[i]);
		}
		for(i in pellets)
		{
			SetPelletSize(pellets[i]);
		}
		DrawMap();
		Draw();
		
	}, 150);
}

function InitGame()
{
	images = ImageLoader.GetImages();
	InitMap();
	InitPacman();	
	InitGhosts();
	CreatePellets();
	DrawMap();
	Draw();
	lastTime = 0;
	UpdateTime();	
	setTimeout(Run, 15);
}

function InitPacman()
{
	bCanMove = {
		right: false,
		left: false,
		up: false,
		down: false
	};

	pacman = {
		ai: false,
		x: 0,
		y: 0,		
		startX: 0,
		startY: 0,
		offsetX: 0,
		offsetY: 0,
		vx: 0,
		vy: 0,
		color: "yellow",
		angle: 0,
		mouthAngle: 0,
		mouthAngleMax: MyMath.PI4,
		mouthAngleMin: 0,
		mouthSpeed: Math.PI / 1000,
		superTimer: 0,
		moveLeft: false,
		moveRight:false,
		moveUp: false,
		moveDown: false,
		lives: 3,
		deadTimer: -1
	};
	
	SetPacmanSize();
}

function SetPacmanSize()
{
	if(mapData.blockWidth > mapData.blockHeight)
		pacman.radius = mapData.blockHalfHeight - 1;
	else
		pacman.radius = mapData.blockHalfWidth - 1;	
	
	pacman.normalSpeedH = mapData.blockWidth / 150;
	pacman.normalSpeedV = mapData.blockHeight / 150;
	pacman.superSpeedH = mapData.blockWidth / 125;
	pacman.superSpeedV = mapData.blockHeight / 125;
	pacman.speedH = pacman.normalSpeedH;
	pacman.speedV = pacman.normalSpeedV;
	
}

function InitGhosts()
{
	ghosts = [];
	for(var x = 0; x < map.length; x++)
	{
		for(var y = 0; y < map[x].length; y++)
		{			
			if(map[x][y] < -3)
			{
				CreateGhost(x, y, images[map[x][y] + 7], images[4]);
			}
		}
	}
}

function CreateGhost(x, y, image, scaredImage)
{	
	var ghost = {
		ai: GhostAI,
		isScared: false,
		x: x,
		y: y,
		startX: x,
		startY: y,
		offsetX: 0,
		offsetY: 0,
		vx: 0,
		vy: 0,
		angle: 0,
		image: image,
		scaredImage: scaredImage,
		moveLeft: false,
		moveRight: true,
		moveUp: false,
		moveDown: false,
		isBlinking: false,
		lastBlink: 0,
		blinkTime: 10,
		blinkImage: image,
		blink: true
	};
	SetGhostSize(ghost);
	
	ghosts.push(ghost);
}

function SetGhostSize(ghost)
{
	ghost.scaleX = mapData.blockWidth / ghost.image.width;
	ghost.scaleY = mapData.blockHeight / ghost.image.height;
	ghost.speedH = mapData.blockWidth / 150;
	ghost.speedV = mapData.blockHeight / 150;
}

function GhostAI(bCanMove)
{
	var possibleMoves = [];
	
	var dx = pacman.x - this.x;
	var dy = pacman.y - this.y;
	var d = Math.sqrt(dx * dx + dy * dy);
	
	if(bCanMove.left && this.vx <= 0)
		possibleMoves.push("left");
	if(bCanMove.right && this.vx >= 0)
		possibleMoves.push("right");
	if(bCanMove.up && this.vy <= 0)
		possibleMoves.push("up");
	if(bCanMove.down && this.vy >= 0)
		possibleMoves.push("down");
	
	var move = "";
	//msgLog.push(d);
	if(d < 6 && possibleMoves.length > 1 && !pacman.isDead)
	{		
		if(this.isScared)
		{
			if(pacman.x < this.x && bCanMove.right && this.vx >= 0)			
				move = "right";
			else if(pacman.x > this.x && bCanMove.left && this.vx <= 0)			
				move = "left";			
			else if(pacman.y < this.y && bCanMove.down && this.vy >= 0)			
				move = "down";
			else if(pacman.y > this.y && bCanMove.up && this.vy <= 0)			
				move = "up";		
		}
		else
		{
			if(pacman.x < this.x && bCanMove.left && this.vx <= 0)			
				move = "left";
			else if(pacman.x > this.x && bCanMove.right && this.vx >= 0)			
				move = "right";			
			else if(pacman.y < this.y && bCanMove.up && this.vy <= 0)			
				move = "up";
			else if(pacman.y > this.y && bCanMove.down && this.vy >= 0)			
				move = "down";		
		}
		if(move != "")
			ClearMovement(this);
	}
	else
	{
		if(possibleMoves.length > 1)
		{
			ClearMovement(this);
			move = possibleMoves[Math.floor(Math.random() * possibleMoves.length)];				
		}
	}
	
	if(move == "left")
		this.moveLeft = true;
	else if(move == "right")
		this.moveRight = true;
	else if(move == "up")
		this.moveUp = true;
	else if(move == "down")
		this.moveDown = true;
}

function ConvertToWorldX(x)
{
	return x * mapData.blockWidth + mapData.blockHalfWidth;
}

function ConvertToWorldY(y)
{
	return y * mapData.blockHeight + mapData.blockHalfHeight;
}

function ConvertToGridX(x)
{
	return Math.round((x - mapData.blockHalfWidth) / mapData.blockWidth);
}

function ConvertToGridY(y)
{
	return Math.round((y - mapData.blockHalfHeight) / mapData.blockHeight);
}

function InitMap()
{	
	map = [
		[1, 1, 1, 1, 1, 1, 1, 1, 1, -1, -1, -1, 1, -1, 1, -1, -1, -1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
		[1, 0, 0, -2, 0, 0, 0, 0, 1, -1, -1, -1, 1, -1, 1, -1, -1, -1, 1, 0, 0, 0, -2, 1, 1, 0, 0, 0, 0, 1],
		[1, 0, 1, 1, 1, 0, 1, 0, 1, -1, -1, -1, 1, -1, 1, -1, -1, -1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1, 0, 1],
		[1, 0, 1, -1, 1, 0, 1, 0, 1, -1, -1, -1, 1, -1, 1, -1, -1, -1, 1, 0, 1, 1, 0, 0, 0, 0, 1, 1, 0, 1],
		[1, 0, 1, -1, 1, 0, 1, 0, 1, -1, -1, -1, 1, -1, 1, -1, -1, -1, 1, 0, 1, 1, 1, 1, 1, 0, 1, 1, 0, 1],
		[1, 0, 1, 1, 1, 0, 1, 0, 1, 1, 1, 1, 1, -1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 1, 1, 0, 1],
		[1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 1],
		[1, 0, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, -1, 1, 1, 1, 1, 1, 0, 1, 1, 0, 1, 1, 1, 1, 1, 0, 1],
		[1, 0, 1, -1, 1, 0, 0, 0, 0, 1, 1, 1, 1, -1, 1, 1, 1, 1, 1, 0, 1, 1, 0, 1, 1, 1, 1, 1, 0, 1],
		[1, 0, 1, -1, 1, 0, 1, 1, 0, 1, 1, -1, -1, -1, -1, -1, -1, -1, -1, 0, 1, 1, 0, 0, 0, 0, 1, 1, 0, 1],
		[1, 0, 1, -1, 1, 0, 1, 1, 0, 1, 1, -1, 1, 1, 1, -1, 1, 1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1, 0, 1],
		[1, 0, 1, -1, 1, 0, 1, 1, 0, 1, 1, -1, 1, -5, 1, -1, 1, 1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1, 0, 1],
		[1, 0, 1, 1, 1, 0, 1, 1, 0, 1, 1, -1, 1, -1, 1, -1, 1, 1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1, 0, 1],
		[1, 0, 0, 0, 0, 0, 1, 1, 0, -1, -1, -1, 1, -7, 1, -1, 1, 1, 1, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 1],
		[1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, -1, 1, -1, 1, -1, 1, 1, 1, 1, 1, 1, -1, 1, 1, 1, 1, 1, 0, 1],
		[1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, -1, -1, -4, 1, -1, 1, 1, 1, 1, 1, 1, -3, 1, 1, 1, 1, 1, 0, 1],
		[1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, -1, 1, -1, 1, -1, 1, 1, 1, 1, 1, 1, -1, 1, 1, 1, 1, 1, 0, 1],
		[1, 0, 0, 0, 0, 0, 1, 1, 0, -1, -1, -1, 1, -6, 1, -1, 1, 1, 1, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 1],
		[1, 0, 1, 1, 1, 0, 1, 1, 0, 1, 1, -1, 1, -1, 1, -1, 1, 1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1, 0, 1],
		[1, 0, 1, -1, 1, 0, 1, 1, 0, 1, 1, -1, 1, -1, 1, -1, 1, 1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1, 0, 1],
		[1, 0, 1, -1, 1, 0, 1, 1, 0, 1, 1, -1, 1, 1, 1, -1, 1, 1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1, 0, 1],
		[1, 0, 1, -1, 1, 0, 1, 1, 0, 1, 1, -1, -1, -1, -1, -1, -1, -1, -1, 0, 1, 1, 0, 0, 0, 0, 1, 1, 0, 1],
		[1, 0, 1, -1, 1, 0, 0, 0, 0, 1, 1, 1, 1, -1, 1, 1, 1, 1, 1, 0, 1, 1, 0, 1, 1, 1, 1, 1, 0, 1],
		[1, 0, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, -1, 1, 1, 1, 1, 1, 0, 1, 1, 0, 1, 1, 1, 1, 1, 0, 1],
		[1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 1],
		[1, 0, 1, 1, 1, 0, 1, 0, 1, 1, 1, 1, 1, -1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 1, 1, 0, 1],
		[1, 0, 1, -1, 1, 0, 1, 0, 1, -1, -1, -1, 1, -1, 1, -1, -1, -1, 1, 0, 1, 1, 1, 1, 1, 0, 1, 1, 0, 1],
		[1, 0, 1, -1, 1, 0, 1, 0, 1, -1, -1, -1, 1, -1, 1, -1, -1, -1, 1, 0, 1, 1, 0, 0, 0, 0, 1, 1, 0, 1],
		[1, 0, 1, 1, 1, 0, 1, 0, 1, -1, -1, -1, 1, -1, 1, -1, -1, -1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1, 0, 1],
		[1, 0, 0, -2, 0, 0, 0, 0, 1, -1, -1, -1, 1, -1, 1, -1, -1, -1, 1, 0, 0, 0, -2, 1, 1, 0, 0, 0, 0, 1],
		[1, 1, 1, 1, 1, 1, 1, 1, 1, -1, -1, -1, 1, -1, 1, -1, -1, -1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
	];	
	
	//var gradient = context.createLinearGradient(0,0,canvas.width,canvas.height);
	var gradient = contextMap.createRadialGradient(canvas.width / 2, canvas.height / 2, canvas.width / 35, canvas.width / 2, canvas.height / 2, canvas.width / 2);
	gradient.addColorStop(1,"rgb(0, 25, 150)");
	gradient.addColorStop(0,"rgb(0, 75, 225)");
 
	var blockWidth = Math.round(canvasMap.width / map.length);
	var blockHeight = Math.round(canvasMap.height / map[0].length); 
	mapData = {
		blockWidth: blockWidth,
		blockHeight: blockHeight,
		blockHalfWidth: blockWidth / 2,
		blockHalfHeight: blockHeight / 2,
		color: gradient,
		doorColor: "rgb(85, 125, 235)",
		map: map
	};
}

function CreatePellets()
{
	pellets = [];
	var map = mapData.map;
	contextMap.fillStyle = mapData.color;
	for(var x = 0; x < map.length; x++)
	{
		for(var y = 0; y < map[x].length; y++)
		{
			var x2 = x * mapData.blockWidth;
			var y2 = y * mapData.blockHeight;
			
			if(map[x][y] == 0 || map[x][y] == -2)
			{
				var isSuper = false;
							
				
				var score = 20;
				
				if(map[x][y] == -2)
				{
					isSuper = true;
					
					score = 100;
				}
			
				var pellet = {
					gridX: x,
					gridY: y,
					isSuper: isSuper,
					color: "rgb(215, 215, 100)",
					score: score
				};
				SetPelletSize(pellet);
				pellets.push(pellet);
			}
			else if(map[x][y] == -3)
			{
				pacman.x = x;
				pacman.y = y;
				pacman.startX = x;
				pacman.startY = y;
			}			
		}
	}
}

function SetPelletSize(pellet)
{
	pellet.width = mapData.blockWidth / 4;
	pellet.height = mapData.blockHeight / 4;
	if(pellet.isSuper)
	{
		pellet.width = mapData.blockWidth / 2;
		pellet.height = mapData.blockHeight / 2;		
	}
	pellet.halfWidth = pellet.width / 2;
	pellet.halfHeight = pellet.height / 2;
	
}

function UpdateTime()
{
	var d = new Date();
	t = d.getTime();
	dt = t - lastTime;
	lastTime = t;
}

function Run()
{
	msgLog = [];
	UpdateTime();
	MoveObjects();
	SuperTimer();	
	DetectPellets();
	DetectGhosts();
	Draw();	
	ProcessFloatingTexts();
	DetectWin();	
	DeadTimer();
	
	DrawTopBar();
	DisplayMessages();
	setTimeout(Run, 15);
}

function ProcessFloatingTexts()
{
	var removeFloatingTexts = [];
	for(var i = 0; i < floatingTexts.length; i++)
	{
		var ft = floatingTexts[i];		
		context.font = "bold 16px Verdana";
		context.fillStyle = "white";
		context.fillText(ft.txt, ft.x - context.measureText(ft.txt).width / 2, ft.y + 8);
		
		ft.life--;
		if(ft.life <= 0)
		{
			removeFloatingTexts.push(i);
		}		
	}
	
	for(i = removeFloatingTexts.length - 1; i >= 0; i--)
	{
		floatingTexts.splice(i, 1);
	}
}

function DisplayMessages()
{
	var strMsg = "";
	for(var i = 0; i < msgLog.length; i++)
	{
		strMsg += msgLog[i] + "<br />";
	}
	
	document.getElementById("msg").innerHTML = strMsg;
}

function MoveObjects()
{
	if(!pacman.isDead)
		MoveObject(pacman, true);
		
	for(var i = 0; i < ghosts.length; i++)
	{
		if(ghosts[i].isDead)
			MoveToOrigin(ghosts[i]);
		else
			MoveObject(ghosts[i]);
	}
}

function MoveToOrigin(obj)
{
	var dx = obj.startX - obj.x;
	var dy = obj.startY - obj.y;
	var dx2 = 0 - obj.offsetX;
	var dy2 = 0 - obj.offsetY;
	
	var a = Math.atan2(dy, dx);
	var a2 = Math.atan2(dy2, dx2);
	
	var vAlign = false;
	var hAlign = false;
	if(Math.abs(dx) < 0.2)	
	{
		hAlign = true;
		obj.x = obj.startX;	
	}
	else
		obj.x += Math.cos(a) * 0.005 * dt;
	
	if(Math.abs(dy) < 0.2)
	{
		vAlign = true;
		obj.y = obj.startY;
	}
	else
		obj.y += Math.sin(a) * 0.005 * dt;	
	
	if(Math.abs(obj.offsetX) < 0.1)
		obj.offsetX = 0;
	else
		obj.offsetX += Math.cos(a2) * 0.1 * dt;
	
	if(Math.abs(obj.offsetY) < 0.1)
		obj.offsetY = 0;
	else
		obj.offsetY += Math.sin(a2) * 0.1 * dt;
		
	if(vAlign && hAlign)
	{
		obj.isDead = false;
		obj.isScared = false;
		obj.isBlinking = false;
		obj.offsetX = 0;
		obj.offsetY = 0;
	}
}

function MoveObject(obj, isPacman)
{	
	UpdatePossibleMoves(obj);
	if(obj.vx > 0)
	{
		obj.angle = 0;
		if(!bCanMove.right)
		{
			ClearVelocity(obj);	
			ClearMovement(obj);
			if( ! isPacman ) {
				if(bCanMove.up)
					obj.moveUp = true;
				else if(bCanMove.down)
					obj.moveDown = true;
				else
					obj.moveLeft = true;
			}
		}
		else
		{
			obj.offsetX += obj.vx * dt;
			if(obj.offsetX >= mapData.blockWidth)
			{
				obj.offsetX = 0;
				obj.x++;
				if(obj.x >= map.length)
					obj.x = 0;
			}
		}
	}	
	else if(obj.vx < 0)
	{				
		obj.angle = Math.PI;
		if(!bCanMove.left)
		{
			ClearVelocity(obj);
			ClearMovement(obj);
			if( ! isPacman ) {
				if(bCanMove.up)
					obj.moveUp = true;
				else if(bCanMove.down)
					obj.moveDown = true;
				else
					obj.moveRight = true;
			}
		}
		else
		{
			obj.offsetX += obj.vx * dt;
			if(obj.offsetX <= -mapData.blockWidth)
			{
				obj.offsetX = 0;
				obj.x--;
				if(obj.x < 0)
					obj.x = map.length - 1;
			}
		}
	}
	else if(obj.vy > 0)
	{		
		obj.angle = MyMath.PI2;
		if(!bCanMove.down)
		{
			ClearVelocity(obj);		
			ClearMovement(obj);
			if( ! isPacman ) {
				if(bCanMove.left)
					obj.moveLeft = true;
				else if(bCanMove.right)
					obj.moveRight = true;
				else
					obj.moveUp = true;
			}
		}
		else
		{
			obj.offsetY += obj.vy * dt;
			if(obj.offsetY >= mapData.blockWidth)
			{
				obj.offsetY = 0;
				obj.y++;
				if(obj.y >= map[0].length)
					obj.y = 0;
			}
		}
	}
	else if(obj.vy < 0)
	{
		obj.angle = MyMath.THREEPI2;
		if(!bCanMove.up)
		{
			ClearVelocity(obj);		
			ClearMovement(obj);
			if( ! isPacman ) {
				if(bCanMove.left)
					obj.moveLeft = true;
				else if(bCanMove.right)
					obj.moveRight = true;
				else
					obj.moveDown = true;
			}
		}
		else
		{
			obj.offsetY += obj.vy * dt;
			if(obj.offsetY <= -mapData.blockWidth)
			{
				obj.offsetY = 0;
				obj.y--;
				if(obj.y < 0)
					obj.y = map[0].length - 1;
			}
		}
	}
	
	UpdatePossibleMoves(obj);
	
	if(obj.ai)
		obj.ai(bCanMove);
		
	if(obj.moveLeft && obj.offsetY == 0  && bCanMove.left )
	{
		if(obj.vx > 0)
		{
			obj.x++;
			if(obj.x >= map.length)
				obj.x = 0;
			obj.offsetX = -(mapData.blockWidth - obj.offsetX);
		}
		
		obj.vx = -obj.speedH;
		obj.vy = 0;
	}	
	if(obj.moveRight && obj.offsetY == 0 && bCanMove.right)
	{
		if(obj.vx < 0)
		{
			obj.x--;
			if(obj.x < 0)
				obj.x = map.length - 1;
			obj.offsetX = (mapData.blockWidth + obj.offsetX);
		}
		obj.vx = obj.speedH;
		obj.vy = 0;
	}	
	if(obj.moveUp && obj.offsetX == 0 && bCanMove.up)
	{
		if(obj.vy > 0)
		{
			obj.y++;
			if(obj.y >= map[0].length)
				obj.y = 0;
			obj.offsetY = -(mapData.blockHeight - obj.offsetY);
		}
		obj.vy = -obj.speedV;
		obj.vx = 0;
	}
	if(obj.moveDown && obj.offsetX == 0 && bCanMove.down)
	{
		if(obj.vy < 0)
		{
			obj.y--;
			if(obj.y < 0)
				obj.y = map[0].length - 1;
				
			obj.offsetY = (mapData.blockHeight + obj.offsetY);
		}
		obj.vy = obj.speedV;	
		obj.vx = 0;
	}
}

function SuperTimer()
{
	pacman.superTimer--;
	if(pacman.superTimer == 0)
	{
		scoreMultiple = 0;
		pacman.speedH = pacman.normalSpeedH;
		pacman.speedV = pacman.normalSpeedV;
		if(pacman.vx > 0)
			pacman.vx = pacman.speedH;
		else if(pacman.vx < 0)
			pacman.vx = -pacman.speedH;
		else if(pacman.vy > 0)
			pacman.vy = pacman.speedV;
		else if(pacman.vy > 0)
			pacman.vy = -pacman.speedV;
		for(var j = 0; j < ghosts.length; j++)
		{
			ghosts[j].isScared = false;
			ghosts[j].isBlinking = false;
		}
	}
	else if(pacman.superTimer < 100)
	{
		for(var j = 0; j < ghosts.length; j++)
		{
			if(ghosts[j].isScared)
			{
				//ghosts[j].lastBlink = ghosts[j].blinkTime;
				ghosts[j].isBlinking = true;
			}
		}
	}
}

function DeadTimer()
{
	pacman.deadTimer--;
	if(pacman.deadTimer == 0)
	{		
		pacman.lives--;
		if(pacman.lives > 0)
		{
			pacman.isDead = false;
			ResetLevel();
		}
	}
}

function ResetLevel()
{
	scoreMultiple = 0;
	pacman.angle = 0;
	pacman.x = pacman.startX;
	pacman.y = pacman.startY;
	pacman.offsetX = 0;
	pacman.offsetY = 0;
	ClearMovement(pacman);
	ClearVelocity(pacman);

	for(var i = 0; i < ghosts.length; i++)
	{
		var ghost = ghosts[i];
		ghost.x = ghost.startX;
		ghost.y = ghost.startY;
		ghost.offsetX = 0;
		ghost.offsetY = 0;
		ghost.isDead = false;
		ghost.isScared = false;
		ghost.isBlinking = false;
		ClearMovement(ghost);
		ClearVelocity(ghost);		
		ghost.moveRight = true;
	}
}

function ClearMovement(obj)
{
	obj.moveLeft = false;
	obj.moveRight = false;
	obj.moveUp = false;
	obj.moveDown = false;	
}

function ClearVelocity(obj)
{
	obj.vx = 0;
	obj.vy = 0;
}

function UpdatePossibleMoves(obj)
{
	bCanMove.right = (obj.x + 1 < map.length && map[obj.x + 1][obj.y] <= 0) || (obj.x + 1 >= map.length && map[0][obj.y] <= 0);
	bCanMove.left = (obj.x - 1 >= 0 && map[obj.x - 1][obj.y] <= 0) || (obj.x - 1 < 0 && map[map.length - 1][obj.y] <= 0);
	bCanMove.down = (obj.y + 1 < map[0].length && map[obj.x][obj.y + 1] <= 0) || (obj.y + 1 >= map[0].length && map[obj.x][0] <= 0);
	bCanMove.up = (obj.y - 1 >= 0 && map[obj.x][obj.y - 1] <= 0) || (obj.y - 1 < 0 && map[obj.x][map[0].length - 1] <= 0);	
}

function DetectPellets()
{
	var removePellets = [];
	for(var i = 0; i < pellets.length; i++)
	{
		if(pacman.x == pellets[i].gridX && pacman.y == pellets[i].gridY)
		{
			removePellets.push(i);
			score += pellets[i].score;
			if(pellets[i].isSuper)
			{
				pacman.superTimer = 500;
				pacman.speedH = pacman.superSpeedH;
				pacman.speedV = pacman.superSpeedV;
				if(pacman.vx > 0)
					pacman.vx = pacman.speedH;
				else if(pacman.vx < 0)
					pacman.vx = -pacman.speedH;
				else if(pacman.vy > 0)
					pacman.vy = pacman.speedV;
				else if(pacman.vy > 0)
					pacman.vy = -pacman.speedV;
				for(var j = 0; j < ghosts.length; j++)
				{
					ghosts[j].isScared = true;
					ghosts[j].isBlinking = false;
				}
			}
		}
	}
	
	for(var i = removePellets.length - 1; i >= 0; i--)
	{
		pellets.splice(removePellets[i], 1);
	}
}

function DetectGhosts()
{
	if(!pacman.isDead)
	{
		var pacObj = {
			x: ConvertToWorldX(pacman.x) + pacman.offsetX,
			y: ConvertToWorldY(pacman.y) + pacman.offsetY,
			r: pacman.radius
		};
			
		for(var i = 0; i < ghosts.length; i++)
		{
			var ghost = ghosts[i];
			
			var ghostObj = {
				x: ConvertToWorldX(ghost.x) + ghost.offsetX,
				y: ConvertToWorldY(ghost.y) + ghost.offsetY,
				r: pacman.radius
			};
			if(!ghost.isDead && DetectCollision(pacObj, ghostObj))
			{
				if(ghost.isScared)
				{
					ghost.isDead = true;
					var scoreAdd = 100 * Math.pow(2, scoreMultiple++);
					score += scoreAdd;
					floatingTexts.push({
						x: ConvertToWorldX(ghost.x) + ghost.offsetX,
						y: ConvertToWorldY(ghost.y) + ghost.offsetY,
						life: 100,
						txt: "" + scoreAdd
					});
				}
				else
				{
					pacman.deadTimer = 100;
					pacman.isDead = true;
				}
			}
		}
	}
}

function DetectCollision(obj1, obj2)
{
	var dx = obj1.x - obj2.x;
	var dy = obj1.y - obj2.y;
	var d = Math.sqrt(dx * dx + dy * dy);
	return d < obj1.r + obj2.r;
}

function DetectWin()
{
	if(pellets.length == 0)
	{
		//alert("You Win!");
		//InitGame();
		ResetLevel();
	}
}

function Draw()
{
	//context.clearRect(0, 0, 3000, 3000);	
	context.clearRect(0, 0, canvas.width, canvas.height);
	//context.fillRect(0, 0, canvas.width, canvas.height);
	DrawPellets();
	DrawPacman();
	DrawGhosts();
}

function DrawPellets()
{	
	for(var i = 0; i < pellets.length; i++)
	{
		context.fillStyle = pellets[i].color;
		context.fillRect(ConvertToWorldX(pellets[i].gridX) - pellets[i].halfWidth, ConvertToWorldY(pellets[i].gridY) - pellets[i].halfHeight, pellets[i].width, pellets[i].height);
	}
}

function DrawPacman()
{
	if(pacman.isDead)
	{
		pacman.mouthAngle += Math.abs(pacman.mouthSpeed) * dt;
		if(pacman.mouthAngle >= Math.PI)
		{
			pacman.mouthAngle = Math.PI;
		}
	}
	else
	{
		pacman.mouthAngle += pacman.mouthSpeed * dt;
		
		if(pacman.mouthAngle >= pacman.mouthAngleMax)
		{
			pacman.mouthSpeed *= -1;	
			pacman.mouthAngle = pacman.mouthAngleMax;
		}
		
		if(pacman.mouthAngle <= pacman.mouthAngleMin)
		{
			pacman.mouthSpeed *= -1;
			pacman.mouthAngle = pacman.mouthAngleMin;
		}
	}
	
	context.save();
	context.beginPath();
	context.fillStyle = pacman.color;
	context.strokeStyle = pacman.color;
	var x = ConvertToWorldX(pacman.x) + pacman.offsetX;
	var y = ConvertToWorldY(pacman.y) + pacman.offsetY;	
	context.translate(x, y);
	context.rotate(pacman.angle);
	context.moveTo(Math.cos(pacman.mouthAngle) * pacman.radius, Math.sin(pacman.mouthAngle) * pacman.radius);
	context.lineTo(0, 0);	
	context.arc(0, 0, pacman.radius, pacman.mouthAngle, MyMath.TWOPI - pacman.mouthAngle);
	context.lineTo(0, 0);
	context.fill();
	context.restore();
}

function DrawTopBar()
{
	for(var i = 0; i < pacman.lives - 1; i++)
	{
		context.save();
		context.beginPath();
		context.fillStyle = pacman.color;
		context.strokeStyle = pacman.color;
		var x = canvas.width - (mapData.blockWidth * (i+1));
		var y = mapData.blockHeight / 2;	
		var a = MyMath.PI6;
		context.translate(x, y);		
		context.moveTo(Math.cos(a) * pacman.radius, Math.sin(a) * pacman.radius);
		context.lineTo(0, 0);	
		context.arc(0, 0, pacman.radius, a, MyMath.TWOPI - a);
		context.lineTo(0, 0);
		context.fill();
		//context.stroke();
		context.restore();
	}
	
	context.font = "bold 16px Verdana";
	context.fillStyle = "white";
	context.fillText(score, canvas.width / 2 - context.measureText(score).width / 2, mapData.blockHeight / 2 + 8);	
}

function DrawGhosts()
{
	for(var i = 0; i < ghosts.length; i++)
	{
		var ghost = ghosts[i];
		var x = ConvertToWorldX(ghost.x) + ghost.offsetX;
		var y = ConvertToWorldY(ghost.y) + ghost.offsetY;
		var dx = pacman.x - ghost.x;
		var dy = pacman.y - ghost.y;
		var eyeAngle = Math.atan2(dy, dx);
		var eyeStartX = ghost.image.halfWidth - 23;
		var eyeStartY = ghost.image.halfHeight - 20;
		var eyeX = eyeStartX + Math.cos(eyeAngle) * 2;
		var eyeY = eyeStartY + Math.sin(eyeAngle) * 2;
		var image = ghost.image;
		if(ghost.isScared)
			image = ghost.scaredImage;
			
		context.save();
		context.translate(x, y);
		context.scale(ghost.scaleX, ghost.scaleY);
		
		if(ghost.isDead)
		{
			context.fillStyle = "white";
			context.fillRect(eyeStartX - 2, eyeStartY - 4, 8, 12);
			context.fillRect(eyeStartX - 4, eyeStartY - 2, 12, 8);
			
			context.fillRect(eyeStartX + 11, eyeStartY - 4, 8, 12);
			context.fillRect(eyeStartX + 9, eyeStartY - 2, 12, 8);
		}
		else if(ghost.isBlinking)
		{
			context.drawImage(ghost.blinkImage, -ghost.blinkImage.halfWidth, -ghost.blinkImage.halfHeight);
			ghost.lastBlink--;
			if(ghost.lastBlink <= 0)
			{
				if(ghost.blink)
					ghost.blinkImage = ghost.scaredImage;
				else
					ghost.blinkImage = ghost.image;
				
				ghost.blink = !ghost.blink;
				ghost.lastBlink = ghost.blinkTime;
			}
		}
		else
		{
			context.drawImage(image, -image.halfWidth, -image.halfHeight);
		}	
			
		if(!ghost.isScared || ghost.isDead)
		{
			context.fillStyle = "blue";
			context.fillRect(eyeX, eyeY, 5, 5);
			context.fillRect(eyeX + 14, eyeY, 5, 5);
		}
		context.restore();
	}
}

function DrawMap()
{
	var map = mapData.map;
	
	for(var x = 0; x < map.length; x++)
	{
		for(var y = 0; y < map[x].length; y++)
		{
			var x2 = x * mapData.blockWidth;
			var y2 = y * mapData.blockHeight;
			
			if(map[x][y] == 1)
			{
				contextMap.fillStyle = mapData.color;
				contextMap.fillRect(x2, y2, mapData.blockWidth, mapData.blockHeight);
			}
			else if(map[x][y] == 2)
			{
				contextMap.fillStyle = mapData.doorColor;
				contextMap.fillRect(x2, y2, mapData.blockWidth, mapData.blockHeight);
			}
		}
	}
}