
function KeyDown(e)
{		
	SetAction(e.keyCode, true);
}

function KeyUp(e)
{
	//document.getElementById("stats").innerHTML = e.keyCode;
	SetAction(e.keyCode, false);
}

function SetAction(keyCode, bOn)
{		
	ClearMovement(pacman);
	switch(keyCode)
	{
		//Left
		case 65:
		case 37:
		case 100:
			pacman.moveLeft = bOn;
			//pacman.moveRight = false;
			break;
		//Right
		case 68:
		case 39:
		case 102:
			pacman.moveRight = bOn;
			//pacman.moveLeft = false;
			break;
		//Up
		case 87:
		case 38:
		case 104:
			pacman.moveUp = bOn;
			//.moveDown = false;
			break;	
		//Down
		case 40:
		case 83:
			pacman.moveDown = bOn;
			//pacman.moveUp = false;
			break;
	}
}

